package htbla.aud3.graphtheory;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * @author TODO Bitte Gruppenmitglieder eintragen!
 */
public class Graph {
    List<Node> nodes = new ArrayList<>();

    public void read(File adjacencyMatrix) {
        try {
            FileReader in = new FileReader(adjacencyMatrix);
            BufferedReader reader = new BufferedReader(in);
            int nodeID = 0;
            String line = reader.readLine();
            while(line != null) {
                Node node = new Node(nodeID);
                String[] distancesToNode = line.split(";");

                for(int i = 0; i < distancesToNode.length; i++) {
                    int distance = Integer.parseInt(distancesToNode[i]);
                    if(distance != 0) {
                        Edge edge = new Edge(nodeID, i, distance);
                        node.addEdge(edge);
                    }
                }

                nodes.add(node);
                nodeID++;
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {

        } catch (IOException e) {

        }
    }
    
    public Path determineShortestPath(int sourceNodeId, int targetNodeId) {
        return null;
    }
    
    public Path determineShortestPath(int sourceNodeId, int targetNodeId, int... viaNodeIds) {
        return null;
    }
    
    public double determineMaximumFlow(int sourceNodeId, int targetNodeId) {
        return -1.0;
    }
    
    public List<Edge> determineBottlenecks(int sourceNodeId, int targetNodeId) {
        return null;
    }

    class Node {
        int id;
        List<Edge> edges = new ArrayList<>();

        public Node(int id) {
            this.id = id;
        }

        public void addEdge(Edge edge) {
            edges.add(edge);
        }
    }

}
