package htbla.aud3.graphtheory;

import java.io.File;

/**
 * @author Torsten Welsch
 */
public class Main {
    
    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.read(new File("Linz_Suchproblem.csv"));
        int i = 0;
        for(Graph.Node node : graph.nodes){
            System.out.println(i + " ");
            for(Edge edge : node.edges) {
                System.out.println(edge.fromNodeID + " to " + edge.toNodeID + "->  dist. "+ edge.distance);
            }
            System.out.println();
            i++;
        }
    }
    
}
