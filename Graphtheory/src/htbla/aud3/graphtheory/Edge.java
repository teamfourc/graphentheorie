package htbla.aud3.graphtheory;

/*
 * @author TODO Bitte Gruppenmitglieder eintragen!
 */
public class Edge {
    int fromNodeID;
    int toNodeID;
    int distance;

    public Edge(int fromNodeID, int toNodeID, int distance) {
        this.fromNodeID = fromNodeID;
        this.toNodeID = toNodeID;
        this.distance = distance;
    }
    public int getFromNodeId() {
        return -1;
    }
    
    public int getToNodeId() {
        return -1;
    }

}
